/*
 Navicat Premium Data Transfer

 Source Server         : 5.7
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : hserver_db

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 27/06/2020 12:14:09
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_permission`;
CREATE TABLE `sys_permission`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `parent_id` int(11) DEFAULT NULL COMMENT '父id',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单标题',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '路径',
  `component` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '组件',
  `component_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '组件名字',
  `redirect` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '一级菜单跳转地址',
  `menu_type` int(11) DEFAULT NULL COMMENT '菜单类型(0:一级菜单; 1:子菜单:2:按钮权限)',
  `perms` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单权限编码',
  `sort_no` int(10) DEFAULT NULL COMMENT '菜单排序',
  `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单图标',
  `is_route` tinyint(1) DEFAULT 1 COMMENT '是否路由菜单: 0:不是  1:是（默认值1）',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '描述',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  `del_flag` int(1) DEFAULT 0 COMMENT '删除状态 0正常 1已删除',
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_prem_pid`(`parent_id`) USING BTREE,
  INDEX `index_prem_sort_no`(`sort_no`) USING BTREE,
  INDEX `index_prem_del_flag`(`del_flag`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 75 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_permission
-- ----------------------------
INSERT INTO `sys_permission` VALUES (1, NULL, '首页', '/dashboard/analysis', 'dashboard/Analysis', NULL, NULL, 0, NULL, NULL, 'home', 1, NULL, NULL, NULL, NULL, NULL, 0, '/dashboard/analysis');
INSERT INTO `sys_permission` VALUES (3, 7, '用户管理', '/isystem/user', 'system/UserList', NULL, NULL, 1, NULL, 2, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, '/isystem/user');
INSERT INTO `sys_permission` VALUES (4, 7, '菜单管理', '/isystem/newPermissionList', 'system/PermissionList', NULL, NULL, 1, NULL, 3, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, '/isystem/newPermissionList');
INSERT INTO `sys_permission` VALUES (5, 7, '角色管理', '/isystem/roleUserList', 'system/RoleList', NULL, NULL, 1, NULL, 4, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, '/isystem/roleUserList');
INSERT INTO `sys_permission` VALUES (7, NULL, '系统管理', '/isystem', 'layouts/RouteView', NULL, NULL, 0, NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, '/isystem');
INSERT INTO `sys_permission` VALUES (9, 7, '基本配置', '/isystem/confg', 'system/Config', NULL, NULL, 1, NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, '/isystem/confg');
INSERT INTO `sys_permission` VALUES (11, NULL, 'API接口', '/api/apidoc', 'api/apidoc', NULL, NULL, 0, NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, '/api/apidoc');
INSERT INTO `sys_permission` VALUES (40, NULL, '权限管理按钮', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL);
INSERT INTO `sys_permission` VALUES (41, NULL, '用户管理按钮', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL);
INSERT INTO `sys_permission` VALUES (42, NULL, '角色管理按钮', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL);
INSERT INTO `sys_permission` VALUES (43, NULL, '配置管理按钮', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL);
INSERT INTO `sys_permission` VALUES (44, NULL, '测试管理按钮', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL);
INSERT INTO `sys_permission` VALUES (45, 40, '批量删除菜单或权限', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/permission/deleteBatch');
INSERT INTO `sys_permission` VALUES (46, 41, '删除用户', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/user/delete');
INSERT INTO `sys_permission` VALUES (47, 42, '查询所有角色', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/role/queryall');
INSERT INTO `sys_permission` VALUES (48, 41, '检查用户是否存在', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/user/checkOnlyUser');
INSERT INTO `sys_permission` VALUES (49, 42, '删除角色', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/role/delete');
INSERT INTO `sys_permission` VALUES (50, 42, '批量角色', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/role/deleteBatch');
INSERT INTO `sys_permission` VALUES (51, 40, '删除菜单或权限', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/permission/delete');
INSERT INTO `sys_permission` VALUES (52, 40, '查询角色权限', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/permission/queryRolePermission');
INSERT INTO `sys_permission` VALUES (53, 42, '查询角色信息', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/role/queryTreeList');
INSERT INTO `sys_permission` VALUES (54, 41, '批量删除用户', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/user/deleteBatch');
INSERT INTO `sys_permission` VALUES (55, 41, '查询用户角色', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/user/queryUserRole');
INSERT INTO `sys_permission` VALUES (56, 41, '用户列表', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/user/list');
INSERT INTO `sys_permission` VALUES (57, 42, '角色列表', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/role/list');
INSERT INTO `sys_permission` VALUES (58, 40, '权限列表', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/permission/list');
INSERT INTO `sys_permission` VALUES (59, 40, '查询菜单或权限', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/permission/queryTreeList');
INSERT INTO `sys_permission` VALUES (60, 43, '缓存获取', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/config/get');
INSERT INTO `sys_permission` VALUES (61, 44, 'hello测试', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/hello');
INSERT INTO `sys_permission` VALUES (62, 41, '添加用户', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/user/add');
INSERT INTO `sys_permission` VALUES (63, 40, '编辑菜单或权限', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/permission/edit');
INSERT INTO `sys_permission` VALUES (64, 42, '编辑角色', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/role/edit');
INSERT INTO `sys_permission` VALUES (65, 43, '缓存编辑', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/config/edit');
INSERT INTO `sys_permission` VALUES (66, 40, '保存角色权限', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/permission/saveRolePermission');
INSERT INTO `sys_permission` VALUES (67, 41, '编辑用户', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/user/edit');
INSERT INTO `sys_permission` VALUES (68, 40, '添加菜单或权限', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/permission/add');
INSERT INTO `sys_permission` VALUES (69, 42, '添加角色', NULL, NULL, NULL, NULL, 2, NULL, 20, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '/admin/role/add');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `role_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '角色名称',
  `role_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色编码',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '描述',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `index_role_code`(`role_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '管理员', 'admin', '管理员角色', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sys_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_permission`;
CREATE TABLE `sys_role_permission`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL COMMENT '角色id',
  `permission_id` int(11) DEFAULT NULL COMMENT '权限id',
  `data_rule_ids` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_group_role_per_id`(`role_id`, `permission_id`) USING BTREE,
  INDEX `index_group_role_id`(`role_id`) USING BTREE,
  INDEX `index_group_per_id`(`permission_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 199 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_permission
-- ----------------------------
INSERT INTO `sys_role_permission` VALUES (162, 1, 1, NULL);
INSERT INTO `sys_role_permission` VALUES (163, 1, 7, NULL);
INSERT INTO `sys_role_permission` VALUES (164, 1, 11, NULL);
INSERT INTO `sys_role_permission` VALUES (165, 1, 40, NULL);
INSERT INTO `sys_role_permission` VALUES (166, 1, 41, NULL);
INSERT INTO `sys_role_permission` VALUES (167, 1, 42, NULL);
INSERT INTO `sys_role_permission` VALUES (168, 1, 43, NULL);
INSERT INTO `sys_role_permission` VALUES (169, 1, 44, NULL);
INSERT INTO `sys_role_permission` VALUES (170, 1, 3, NULL);
INSERT INTO `sys_role_permission` VALUES (171, 1, 4, NULL);
INSERT INTO `sys_role_permission` VALUES (172, 1, 5, NULL);
INSERT INTO `sys_role_permission` VALUES (173, 1, 9, NULL);
INSERT INTO `sys_role_permission` VALUES (174, 1, 45, NULL);
INSERT INTO `sys_role_permission` VALUES (175, 1, 51, NULL);
INSERT INTO `sys_role_permission` VALUES (176, 1, 52, NULL);
INSERT INTO `sys_role_permission` VALUES (177, 1, 58, NULL);
INSERT INTO `sys_role_permission` VALUES (178, 1, 59, NULL);
INSERT INTO `sys_role_permission` VALUES (179, 1, 63, NULL);
INSERT INTO `sys_role_permission` VALUES (180, 1, 66, NULL);
INSERT INTO `sys_role_permission` VALUES (181, 1, 68, NULL);
INSERT INTO `sys_role_permission` VALUES (182, 1, 46, NULL);
INSERT INTO `sys_role_permission` VALUES (183, 1, 48, NULL);
INSERT INTO `sys_role_permission` VALUES (184, 1, 54, NULL);
INSERT INTO `sys_role_permission` VALUES (185, 1, 55, NULL);
INSERT INTO `sys_role_permission` VALUES (186, 1, 56, NULL);
INSERT INTO `sys_role_permission` VALUES (187, 1, 62, NULL);
INSERT INTO `sys_role_permission` VALUES (188, 1, 67, NULL);
INSERT INTO `sys_role_permission` VALUES (189, 1, 47, NULL);
INSERT INTO `sys_role_permission` VALUES (190, 1, 49, NULL);
INSERT INTO `sys_role_permission` VALUES (191, 1, 50, NULL);
INSERT INTO `sys_role_permission` VALUES (192, 1, 53, NULL);
INSERT INTO `sys_role_permission` VALUES (193, 1, 57, NULL);
INSERT INTO `sys_role_permission` VALUES (194, 1, 64, NULL);
INSERT INTO `sys_role_permission` VALUES (195, 1, 69, NULL);
INSERT INTO `sys_role_permission` VALUES (196, 1, 60, NULL);
INSERT INTO `sys_role_permission` VALUES (197, 1, 65, NULL);
INSERT INTO `sys_role_permission` VALUES (198, 1, 61, NULL);

-- ----------------------------
-- Table structure for sys_token
-- ----------------------------
DROP TABLE IF EXISTS `sys_token`;
CREATE TABLE `sys_token`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `token` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'token',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '描述',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  `del_flag` int(1) DEFAULT 0 COMMENT '删除状态 0正常 1已删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 90 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'token表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_token
-- ----------------------------
INSERT INTO `sys_token` VALUES (89, '0d623d3fccea400b9202a01c5d60010e', NULL, 'admin', '2020-06-27 03:24:24', NULL, NULL, 0);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `username` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '登录账号',
  `realname` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '真实姓名',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '密码',
  `birthday` datetime(0) DEFAULT NULL COMMENT '生日',
  `sex` int(11) DEFAULT NULL COMMENT '性别（1：男 2：女）',
  `email` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '电子邮件',
  `phone` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '电话',
  `status` int(2) DEFAULT NULL COMMENT '状态(1：正常  2：冻结 ）',
  `del_flag` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '删除状态（0，正常，1已删除）',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `index_user_name`(`username`) USING BTREE,
  INDEX `index_user_status`(`status`) USING BTREE,
  INDEX `index_user_del_flag`(`del_flag`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'admin', 'admin', 'admin', NULL, NULL, NULL, '13064001744', NULL, '0', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `role_id` int(11) DEFAULT NULL COMMENT '角色id',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index2_groupuu_user_id`(`user_id`) USING BTREE,
  INDEX `index2_groupuu_ole_id`(`role_id`) USING BTREE,
  INDEX `index2_groupuu_useridandroleid`(`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (5, 1, 1);

SET FOREIGN_KEY_CHECKS = 1;
