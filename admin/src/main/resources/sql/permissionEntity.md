cont
===
    select count(*) from sys_permission where 1=1

    @if(!isEmpty(p.id)){
    and id = #p.id#
    @}
    
    @if(!isEmpty(p.name)){
    and name = #p.name#
    @}
    
    @if(!isEmpty(p.url)){
    and url = #p.url#
    @}
    
    @if(!isEmpty(p.menuType)){
    and menuType = #p.menuType#
    @}

list
===
    select * from sys_permission where 1=1
    @if(!isEmpty(p.id)){
    and id = #p.id#
    @}
    
    @if(!isEmpty(p.name)){
    and name = #p.name#
    @}
    
    @if(!isEmpty(p.url)){
    and url = #p.url#
    @}
    
    @if(!isEmpty(p.menuType)){
    and menuType = #p.menuType#
    @}
    
    @if(!isEmpty(p.pageNo)&&!isEmpty(p.pageSize)){
        limit #p.poff#,#p.pageSize#
    @}

getByNameOne
===
        select * from sys_permission where
        name = #name#
        limit 1