list
===
    select * from sys_user where 1=1
    
    @if(!isEmpty(p.id)){
    and id = #p.id#
    @}
    
    @if(!isEmpty(p.username)){
    and username = #p.username#
    @}
    
    @if(!isEmpty(p.password)){
    and password = #p.password#
    @}
    
    @if(!isEmpty(p.status)){
    and status = #p.status#
    @}
    
    @if(!isEmpty(p.phone)){
    and phone = #p.phone#
    @}
    @if(!isEmpty(p.pageNo)&&!isEmpty(p.pageSize)){
        limit #p.poff#,#p.pageSize#
    @}

cont
===
    select count(*) from sys_role where 1=1
    
    @if(!isEmpty(p.id)){
    and id = #p.id#
    @}
    
    @if(!isEmpty(p.username)){
    and username = #p.username#
    @}
    
    @if(!isEmpty(p.password)){
    and password = #p.password#
    @}
    
    @if(!isEmpty(p.status)){
    and status = #p.status#
    @}
    
    @if(!isEmpty(p.phone)){
    and phone = #p.phone#
    @}