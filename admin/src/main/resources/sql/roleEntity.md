list
===
    select * from sys_role where 1=1
    
    @if(!isEmpty(p.id)){
    and id = #p.id#
    @}
    
    @if(!isEmpty(p.roleName)){
    and roleName = #p.roleName#
    @}
    
    @if(!isEmpty(p.roleCode)){
    and roleCode = #p.roleCode#
    @}

    @if(!isEmpty(p.pageNo)&&!isEmpty(p.pageSize)){
        limit #p.poff#,#p.pageSize#
    @}

cont
===
    select count(*) from sys_role where 1=1
    
    @if(!isEmpty(p.id)){
    and id = #p.id#
    @}
    
    @if(!isEmpty(p.roleName)){
    and roleName = #p.roleName#
    @}
    
    @if(!isEmpty(p.roleCode)){
    and roleCode = #p.roleCode#
    @}