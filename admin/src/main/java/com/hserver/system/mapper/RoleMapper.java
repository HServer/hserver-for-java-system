package com.hserver.system.mapper;

import com.hserver.system.entity.RoleEntity;
import org.beetl.sql.core.annotatoin.Param;
import org.beetl.sql.core.annotatoin.Sql;
import org.beetl.sql.core.mapper.BaseMapper;
import top.hserver.core.ioc.annotation.BeetlSQL;

import java.util.List;

@BeetlSQL
public interface RoleMapper extends BaseMapper<RoleEntity> {

  List<RoleEntity> list(@Param("p") RoleEntity roleEntity);

  int cont(@Param("p") RoleEntity roleEntity);

  @Sql("delete from sys_role where id = ?")
  void deleteById(@Param("p") Integer id);

}
