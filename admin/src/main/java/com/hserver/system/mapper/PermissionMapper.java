package com.hserver.system.mapper;

import com.hserver.system.entity.PermissionEntity;
import org.beetl.sql.core.annotatoin.Param;
import org.beetl.sql.core.annotatoin.Sql;
import org.beetl.sql.core.mapper.BaseMapper;
import top.hserver.core.ioc.annotation.BeetlSQL;

import java.util.List;

@BeetlSQL
public interface PermissionMapper extends BaseMapper<PermissionEntity> {

    List<PermissionEntity> list(@Param("p")PermissionEntity permissionEntity);

    PermissionEntity getByNameOne(@Param("name")String name);

    @Sql("select r.id from sys_permission r join sys_role_permission rp on rp.permission_id = r.id where rp.role_id  = ?")
    List<Integer> getIdsByRoleId(@Param("p") Integer valueOf);

    @Sql("delete from sys_role_permission where role_id = ?")
    void deleteRolePermissionByRoleId(@Param("p") Integer r);

    @Sql("insert into sys_role_permission(role_id,permission_id) value(?,?)")
    void saveRolePermission(@Param("p")Integer r, @Param("p1")Integer valueOf);

    int cont(@Param("p")PermissionEntity permissionEntity);

    @Sql("delete from sys_permission where id=?")
    void deleteById(@Param("p")Integer id);

}
