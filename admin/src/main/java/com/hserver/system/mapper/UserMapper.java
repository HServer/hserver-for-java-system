package com.hserver.system.mapper;

import com.hserver.system.entity.PermissionEntity;
import com.hserver.system.entity.RoleEntity;
import com.hserver.system.entity.UserEntity;
import org.beetl.sql.core.annotatoin.Param;
import org.beetl.sql.core.annotatoin.Sql;
import org.beetl.sql.core.mapper.BaseMapper;
import top.hserver.core.ioc.annotation.BeetlSQL;

import java.util.List;

@BeetlSQL
public interface UserMapper extends BaseMapper<UserEntity> {

    @Sql("select * from sys_user where username = ? and password = ?")
    UserEntity login(@Param("p") String username,@Param("p1") String password);

    @Sql("select p.* from sys_permission p where p.id in (" +
            "select distinct p1.id from sys_permission p1 join sys_role_permission rp on p1.id=rp.permission_id " +
            "join sys_user_role ur on ur.role_id = rp.role_id " +
            "join sys_user u on u.id = ur.user_id " +
            "where u.username = ?" +
            ") order by p.sort_no")
    List<PermissionEntity> getUserPermission(@Param("p")String username);

    @Sql("delete from sys_token where create_by = ?")
    void logout(@Param("p")String username);

    int cont(@Param("p")UserEntity userEntity);

    List<UserEntity> list(@Param("p")UserEntity userEntity);

    @Sql("delete from sys_user_role where user_id = ?")
    int delRole(@Param("p")Integer id);

    @Sql("insert into sys_user_role (user_id,role_id)" +
            " value(?,?)")
    void insertRole(@Param("p")Integer userId,@Param("p1")Integer roleId);

    @Sql("delete from sys_user where id = ?")
    void deleteById(@Param("p")Integer id);

    @Sql("select r.* from sys_role r join sys_user_role ur on ur.role_id=r.id where ur.user_id = ?")
    List<RoleEntity> queryRole(@Param("p")Integer userid);

    @Sql("SELECT LAST_INSERT_ID()")
    int getLastId();

}
