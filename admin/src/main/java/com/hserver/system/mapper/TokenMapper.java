package com.hserver.system.mapper;

import com.hserver.system.entity.TokenEntity;
import org.beetl.sql.core.annotatoin.Param;
import org.beetl.sql.core.annotatoin.Sql;
import org.beetl.sql.core.mapper.BaseMapper;
import top.hserver.core.ioc.annotation.BeetlSQL;

@BeetlSQL
public interface TokenMapper extends BaseMapper<TokenEntity> {

    @Sql("select * from sys_token where create_by = ?")
    TokenEntity getToken(@Param("p")String username);

    @Sql("delete from sys_token where create_by = ?")
    void deleteByUsername(@Param("p")String username);

    @Sql("select * from sys_token where token = ?")
    TokenEntity getByToken(@Param("p")String st);
}
