package com.hserver.system.service;

import com.hserver.system.entity.PermissionEntity;
import com.hserver.system.entity.RoleEntity;
import com.hserver.system.mapper.PermissionMapper;
import com.hserver.system.mapper.RoleMapper;
import com.hserver.system.utils.PageView;
import top.hserver.core.ioc.annotation.Autowired;
import top.hserver.core.ioc.annotation.Bean;
import top.hserver.core.server.util.JsonResult;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Bean
public class RoleService {

    @Autowired
    private RoleMapper roleMapper;

    @Autowired
    private PermissionMapper permissionMapper;

    public List<RoleEntity> list(RoleEntity roleEntity) {
        List<RoleEntity> list = roleMapper.list(roleEntity);
        return list;
    }

    public PageView page(RoleEntity roleEntity) {
        int totalCont = roleMapper.cont(roleEntity);
        List<RoleEntity> userEntities=roleMapper.list(roleEntity);
        PageView pageView=PageView.of(totalCont,roleEntity.getPageSize(),roleEntity.getPageNo(),userEntities);
        return pageView;
    }

    public JsonResult add(RoleEntity roleEntity) {
        roleMapper.insert(roleEntity);
        //userMapper.delRoles(userEntity.);
        JsonResult jsonResult=JsonResult.ok();
        jsonResult.put("success",true);
        return jsonResult;
    }

    public JsonResult edit(RoleEntity roleEntity) {
        roleMapper.updateById(roleEntity);
        JsonResult jsonResult=JsonResult.ok();
        jsonResult.put("success",true);
        return jsonResult;
    }

    public JsonResult deleteById(Integer id) {
        roleMapper.deleteById(id);
        JsonResult jsonResult=JsonResult.ok();
        jsonResult.put("success",true);
        return jsonResult;
    }

    public JsonResult deleteBatch(String[] id) {
        if(id!=null){
            for (int i=0;i<id.length;i++){
                if(id[i]!=null&&!"".equals(id[i])){
                    deleteById(Integer.valueOf(id[i]));
                }
            }
        }
        JsonResult jsonResult=JsonResult.ok();
        jsonResult.put("success",true);
        return jsonResult;
    }


    public void buildRef(Map evalEntity, List<Map> evalEntities){
        boolean lv=true;
        for (int i=0;i<evalEntities.size();i++){
            Map ee = evalEntities.get(i);
            if(ee.get("parentId")!=null){
                if(ee.get("parentId").equals(evalEntity.get("id"))){
                    lv=false;
                    evalEntity.put("isLeaf",false);
                    if(!evalEntity.containsKey("children")){
                        evalEntity.put("children",new ArrayList<>());
                    }
                    ((List)evalEntity.get("children")).add(ee);
                    buildRef(ee,evalEntities);
                }
            }
        }
    }

    public void removeChild(List<Map> evalEntities){
        for(int i=0;i<evalEntities.size();i++){
            Map ee = evalEntities.get(i);
            if(ee.get("parentId")!=null){
                evalEntities.remove(ee);
                i--;
            }
        }
    }

    public List queryTreeList() {
        List<PermissionEntity> permissionEntities = permissionMapper.list(new PermissionEntity());
        List<Map> list=new ArrayList();
        for(int i=0;i<permissionEntities.size();i++){
            Map map=new HashMap();
            map.put("id",permissionEntities.get(i).getId()+"");
            map.put("key",permissionEntities.get(i).getId()+"");
            map.put("label",permissionEntities.get(i).getName());
            map.put("title",permissionEntities.get(i).getName());
            map.put("slotTitle",permissionEntities.get(i).getName());
            if(permissionEntities.get(i).getParentId()!=null) {
                map.put("parentId", permissionEntities.get(i).getParentId() + "");
            }else {
                map.put("parentId",null);
            }
            map.put("value",permissionEntities.get(i).getId()+"");
            map.put("scopedSlots",new HashMap<String,String>(){{put("title","hasDatarule");}});
            map.put("isLeaf",true);//isLeaf: true
            list.add(map);
        }
        for(int i=0;i<list.size();i++){
            if(list.get(i).get("parentId")==null){
                buildRef(list.get(i),list);
            }
        }
        removeChild(list);
        return list;
    }
}
