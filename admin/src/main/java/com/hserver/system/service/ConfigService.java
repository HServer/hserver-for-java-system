package com.hserver.system.service;

import com.hserver.system.entity.ConfigEntity;
import top.hserver.core.ioc.annotation.Autowired;
import top.hserver.core.ioc.annotation.Bean;


@Bean
public class ConfigService {

    @Autowired
    private ConfigEntity configEntity;

    public String getTokenLocation() {
        return configEntity.getTokenLocation();
    }

    public ConfigEntity getConfig() {
        return configEntity;
    }

    public void edit(ConfigEntity configEntityMe) {
        configEntity = configEntityMe;
    }
}
