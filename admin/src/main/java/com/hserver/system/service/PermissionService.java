package com.hserver.system.service;

import com.hserver.system.entity.PermissionEntity;
import com.hserver.system.mapper.PermissionMapper;
import top.hserver.core.ioc.annotation.Autowired;
import top.hserver.core.ioc.annotation.Bean;
import top.hserver.core.server.util.JsonResult;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Bean
public class PermissionService {

    @Autowired
    private PermissionMapper permissionMapper;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private UserService userService;

    /**
     * 获取用户的权限
     *
     * @param token
     * @return
     */
    public JsonResult getUserPermissionByToken(String token) {

        /**
         * 可以自己设计修改为redis
         */
        String username = tokenService.getUsernameByToken(token);
        List<PermissionEntity> permissionEntities = userService.getUserPermission(username);
        JsonResult jsonResult = JsonResult.ok();
        jsonResult.put("success", true);
        List<PermissionEntity> finalPermissionEntities = permissionEntities;
        jsonResult.put("result", new HashMap() {{
            put("menu", finalPermissionEntities);
            put("allAuth", new ArrayList<>());
            put("auth", new ArrayList<>());
        }});
        return jsonResult;
    }

    public List getIdsByRoleId(Integer valueOf) {
        List<Integer> p = permissionMapper.getIdsByRoleId(valueOf);
        return p;
    }

    public void deleteRolePermission(Integer r) {
        permissionMapper.deleteRolePermissionByRoleId(r);
    }

    public void saveRolePermission(Integer r, Integer valueOf) {
        permissionMapper.saveRolePermission(r, valueOf);
    }

    public void buildRef(PermissionEntity evalEntity, List<PermissionEntity> evalEntities) {
        for (int i = 0; i < evalEntities.size(); i++) {
            PermissionEntity ee = evalEntities.get(i);
            if (ee.getParentId() != null) {
                if (ee.getParentId() == evalEntity.getId()) {
                    if (evalEntity.getChildren() == null) {
                        evalEntity.setChildren(new ArrayList<>());
                    }
                    evalEntity.getChildren().add(ee);
                    buildRef(ee, evalEntities);
                }
            }
        }
    }

    public void removeChild(List<PermissionEntity> evalEntities) {
        for (int i = 0; i < evalEntities.size(); i++) {
            PermissionEntity ee = evalEntities.get(i);
            if (ee.getParentId() != null) {
                evalEntities.remove(ee);
                i--;
            }
        }
    }

    public List<PermissionEntity> page(PermissionEntity permissionEntity) {
        List<PermissionEntity> permissionEntities = permissionMapper.list(permissionEntity);
        for (int i = 0; i < permissionEntities.size(); i++) {
            Map mate = new HashMap();
            mate.put("title", permissionEntities.get(i).getName());
            mate.put("icon", permissionEntities.get(i).getIcon());
            mate.put("keepAlive", true);
            permissionEntities.get(i).setKey(permissionEntities.get(i).getId());
            if (permissionEntities.get(i).getParentId() == null) {
                buildRef(permissionEntities.get(i), permissionEntities);
            }
        }
        removeChild(permissionEntities);
        return permissionEntities;
    }

    public JsonResult add(PermissionEntity permissionEntity) {
        if (permissionEntity.getMenuType() == null) {
            permissionEntity.setMenuType(0);
        }
        if (permissionEntity.getPath() == null) {
            permissionEntity.setPath(permissionEntity.getUrl());
        }
        if (permissionEntity.getName() != null && permissionEntity.getName().trim().length() == 0) {
            return null;
        }
        permissionMapper.insert(permissionEntity);
        JsonResult jsonResult = JsonResult.ok();
        jsonResult.put("success", true);
        return jsonResult;
    }

    public JsonResult edit(PermissionEntity permissionEntity) {
        if (permissionEntity.getMenuType() == null) {
            permissionEntity.setMenuType(0);
        }
        permissionEntity.setPath(permissionEntity.getUrl());
        permissionMapper.updateById(permissionEntity);
        //
        JsonResult jsonResult = JsonResult.ok();
        jsonResult.put("success", true);
        return jsonResult;
    }

    public JsonResult deleteById(Integer id) {
        permissionMapper.deleteById(id);
        JsonResult jsonResult = JsonResult.ok();
        jsonResult.put("success", true);
        return jsonResult;
    }


    public boolean synchronize(PermissionEntity permissionEntity) {
        PermissionEntity byNameOne = permissionMapper.getByNameOne(permissionEntity.getName());
        if (byNameOne != null) {
            permissionEntity.setId(byNameOne.getId());
            permissionMapper.updateById(permissionEntity);
        } else {
            add(permissionEntity);
        }
        return true;
    }

    public void synchronize(String controllerPackageName) {
        PermissionEntity byNameOne = permissionMapper.getByNameOne(controllerPackageName);
        if (byNameOne == null) {
            PermissionEntity permissionEntity = new PermissionEntity();
            permissionEntity.setName(controllerPackageName);
            permissionEntity.setIsRoute(0);
            permissionEntity.setSortNo(20);
            permissionEntity.setMenuType(2);
            add(permissionEntity);
        }
    }

    public Integer synchronizeId(String controllerPackageName) {
        PermissionEntity byNameOne = permissionMapper.getByNameOne(controllerPackageName);
        return byNameOne.getId();
    }

    public JsonResult deleteBatch(String[] id) {
        if (id != null) {
            for (int i = 0; i < id.length; i++) {
                if (id[i] != null && !"".equals(id[i])) {
                    deleteById(Integer.valueOf(id[i]));
                }
            }
        }
        JsonResult jsonResult = JsonResult.ok();
        jsonResult.put("success", true);
        return jsonResult;
    }
}
