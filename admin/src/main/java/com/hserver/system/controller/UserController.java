package com.hserver.system.controller;

import com.hserver.system.entity.UserEntity;
import com.hserver.system.service.UserService;
import com.hserver.system.utils.PageView;
import top.hserver.core.ioc.annotation.*;
import top.hserver.core.ioc.annotation.apidoc.ApiImplicitParams;
import top.hserver.core.server.util.JsonResult;

@Controller(value = "/admin/user",name = "用户管理按钮")
public class UserController {

    @Autowired
    private UserService userService;

    @RequiresPermissions("用户列表")
    @GET("/list")
    public JsonResult userList(UserEntity userEntity) {
        JsonResult jsonResult = JsonResult.ok();
        PageView pageView = userService.page(userEntity);
        jsonResult.put("result", pageView);
        jsonResult.put("success", true);
        return jsonResult;
    }

    @RequiresPermissions("检查用户是否存在")
    @GET("/checkOnlyUser")
    public JsonResult checkUser(String username, String id) {
        JsonResult jsonResult = userService.checkUsername(username);
        if (id != null) {
            jsonResult.put("success", !(boolean) jsonResult.get("success"));
        }
        return jsonResult;
    }

    @RequiresPermissions("添加用户")
    @POST("/add")
    public JsonResult add(UserEntity userEntity) {
        return userService.add(userEntity);
    }

    @RequiresPermissions("编辑用户")
    @POST("/edit")
    public JsonResult edit(UserEntity userEntity) {
        return userService.edit(userEntity);
    }

    @RequiresPermissions("删除用户")
    @GET("/delete")
    public JsonResult delete(Integer id) {
        return userService.deleteById(Integer.valueOf(id));
    }

    @RequiresPermissions("批量删除用户")
    @GET("/deleteBatch")
    public JsonResult deleteBatch(String ids) {
        String[] id = ids.split(",");
        return userService.deleteBatch(id);
    }

    @RequiresPermissions("查询用户角色")
    @GET("/queryUserRole")
    public JsonResult queryUserRole(Integer userid) {
        return userService.queryUserRole(userid);
    }
}
