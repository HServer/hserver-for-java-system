package com.hserver.system.controller;

import com.hserver.system.entity.UserEntity;
import com.hserver.system.service.TokenService;
import com.hserver.system.service.UserService;
import top.hserver.core.ioc.annotation.Autowired;
import top.hserver.core.ioc.annotation.Controller;
import top.hserver.core.ioc.annotation.GET;
import top.hserver.core.ioc.annotation.POST;
import top.hserver.core.server.util.JsonResult;

import java.util.Map;

@Controller(value = "/admin", name = "登录管理按钮")
public class IndexController {

  @Autowired
  private UserService userService;
  @Autowired
  private TokenService tokenService;

  @GET("/")
  public Map index() {
    return JsonResult.ok();
  }

  @POST("/login")
  public JsonResult login(UserEntity userEntity) {
    return userService.login(userEntity);
  }

  @POST("/logout")
  public JsonResult logout(UserEntity userEntity) {
    return userService.logout(userEntity);
  }

  @GET("/duplicate/check")
  public JsonResult check() {
    JsonResult jsonResult = JsonResult.ok();
    jsonResult.put("success", true);
    return jsonResult;
  }

}
