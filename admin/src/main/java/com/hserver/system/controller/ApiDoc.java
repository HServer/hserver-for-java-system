package com.hserver.system.controller;

import com.hserver.Application;
import top.hserver.core.api.ApiResult;
import top.hserver.core.interfaces.HttpResponse;
import top.hserver.core.ioc.annotation.Controller;
import top.hserver.core.ioc.annotation.GET;
import top.hserver.core.server.util.JsonResult;

import java.util.HashMap;
import java.util.List;

@Controller
public class ApiDoc {

    @GET("/api")
    public void api(HttpResponse httpResponse){
        top.hserver.core.api.ApiDoc apiDoc = new top.hserver.core.api.ApiDoc(Application.class);
        try {
            List<ApiResult> apiData = apiDoc.getApiData();
            HashMap<String, Object> stringObjectHashMap = new HashMap();
            stringObjectHashMap.put("data", apiData);
            httpResponse.sendTemplate("hserver_doc.ftl", stringObjectHashMap);
        } catch (Exception var5) {
            httpResponse.sendJson(JsonResult.error());
        }
    }
}