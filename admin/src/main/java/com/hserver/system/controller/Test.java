package com.hserver.system.controller;

import top.hserver.core.ioc.annotation.Controller;
import top.hserver.core.ioc.annotation.GET;
import top.hserver.core.ioc.annotation.RequiresPermissions;
import top.hserver.core.ioc.annotation.apidoc.ApiImplicitParam;
import top.hserver.core.ioc.annotation.apidoc.ApiImplicitParams;
import top.hserver.core.ioc.annotation.apidoc.DataType;
import top.hserver.core.server.util.JsonResult;

@Controller(name = "测试管理按钮")
public class Test {

    @ApiImplicitParams(
            value = {
                    @ApiImplicitParam(name = "name", value = "名字", required = true, dataType = DataType.String),

            },
            note = "这是一个Api的Get方法",
            name = "阿皮获取GET"
    )
    @RequiresPermissions("hello测试")
    @GET("/hello")
    public JsonResult hello(String name) {
        return JsonResult.ok(name);
    }
}
