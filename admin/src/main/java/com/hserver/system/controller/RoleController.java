package com.hserver.system.controller;

import com.hserver.system.entity.PermissionEntity;
import com.hserver.system.entity.RoleEntity;
import com.hserver.system.service.RoleService;
import com.hserver.system.utils.PageView;
import top.hserver.core.ioc.annotation.*;
import top.hserver.core.server.util.JsonResult;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller(value = "/admin/role",name = "角色管理按钮")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @RequiresPermissions("查询所有角色")
    @GET("/queryall")
    public JsonResult all() {
        List<RoleEntity> roleEntities = roleService.list(new RoleEntity());
        JsonResult jsonResult = JsonResult.ok();
        jsonResult.put("success", true);
        jsonResult.put("result", roleEntities);
        return jsonResult;
    }

    @RequiresPermissions("角色列表")
    @GET("/list")
    public JsonResult list(RoleEntity roleEntity) {
        JsonResult jsonResult = JsonResult.ok();
        PageView pageView = roleService.page(roleEntity);
        jsonResult.put("result", pageView);
        jsonResult.put("success", true);
        return jsonResult;
    }


    @RequiresPermissions("添加角色")
    @POST("/add")
    public JsonResult add(RoleEntity roleEntity) {
        return roleService.add(roleEntity);
    }

    @RequiresPermissions("编辑角色")
    @POST("/edit")
    public JsonResult edit(RoleEntity roleEntity) {
        return roleService.edit(roleEntity);
    }

    @RequiresPermissions("删除角色")
    @GET("/delete")
    public JsonResult delete(Integer id) {
        return roleService.deleteById(Integer.valueOf(id));
    }

    @RequiresPermissions("批量角色")
    @GET("/deleteBatch")
    public JsonResult deleteBatch(String ids) {
        String[] id = ids.split(",");
        return roleService.deleteBatch(id);
    }

    @RequiresPermissions("查询角色信息")
    @GET("/queryTreeList")
    public JsonResult queryTreeList() {
        JsonResult jsonResult = JsonResult.ok();
        jsonResult.put("success", true);
        List<PermissionEntity> permissionEntities = roleService.queryTreeList();
        Map treeList = new HashMap();
        treeList.put("treeList", permissionEntities);
        //List<Integer> ids=roleService.getRolePermissionIds();
        treeList.put("ids", new ArrayList<>());
        jsonResult.put("result", treeList);
        return jsonResult;
    }


}
