package com.hserver.system.controller;

import com.hserver.system.entity.ConfigEntity;
import com.hserver.system.service.ConfigService;
import top.hserver.core.ioc.annotation.*;
import top.hserver.core.server.util.JsonResult;

@Controller(value = "/admin/config", name = "配置管理按钮")
public class ConfigController {

  @Autowired
  private ConfigService configService;

  @RequiresPermissions("缓存获取")
  @GET("/get")
  public JsonResult get() {
    ConfigEntity configEntity = configService.getConfig();
    JsonResult jsonResult = JsonResult.ok();
    jsonResult.put("success", true);
    jsonResult.put("data", configEntity);
    return jsonResult;
  }

  @RequiresPermissions("缓存编辑")
  @POST("/edit")
  public JsonResult edit(ConfigEntity configEntity) {
    configService.edit(configEntity);
    JsonResult jsonResult = JsonResult.ok();
    jsonResult.put("success", true);
    jsonResult.put("msg", "修改成功");
    return jsonResult;
  }
}
