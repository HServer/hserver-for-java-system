package com.hserver.system.config;

import com.hserver.system.entity.ConfigEntity;
import com.hserver.system.utils.RedisUtil;
import com.zaxxer.hikari.HikariDataSource;
import org.beetl.sql.core.*;
import org.beetl.sql.core.db.DBStyle;
import org.beetl.sql.core.db.MySqlStyle;
import org.beetl.sql.ext.DebugInterceptor;
import top.hserver.core.ioc.annotation.Bean;
import top.hserver.core.ioc.annotation.Configuration;
import top.hserver.core.ioc.annotation.Value;

/**
 * @author hxm
 */
@Configuration
public class AppConfig {

    @Value("mysql.url")
    private String url;
    @Value("mysql.username")
    private String username;
    @Value("mysql.password")
    private String password;
    @Value("mysql.driver")
    private String driver;


    @Value("token_localhost")
    private String tokenLocalhost;
    @Value("token_expire")
    private Integer tokenExpire;


    @Value("redis.host")
    private String redisHost;
    @Value("redis.port")
    private Integer redisPort;
    @Value("redis.password")
    private String redisPassword;


    @Bean
    public SQLManager sql() {
        HikariDataSource ds = new HikariDataSource();
        ds.setJdbcUrl(url);
        ds.setUsername(username);
        ds.setPassword(password);
        ds.setDriverClassName(driver);
        ConnectionSource source = ConnectionSourceHelper.getSingle(ds);
        DBStyle mysql = new MySqlStyle();

        // sql语句放在classpagth的/sql 目录下
        SQLLoader loader = new ClasspathLoader("/sql");
        // 数据库命名跟java命名一样，所以采用DefaultNameConversion，还有一个是UnderlinedNameConversion，下划线风格的，
        UnderlinedNameConversion nc = new UnderlinedNameConversion();
        // 最后，创建一个SQLManager,DebugInterceptor 不是必须的，但可以通过它查看sql执行情况
        SQLManager sqlManager = new SQLManager(mysql, loader, source, nc, new Interceptor[]{new DebugInterceptor()});
        return sqlManager;
    }

    @Bean
    public ConfigEntity configEntity() {
        ConfigEntity configEntity = new ConfigEntity();
        configEntity.setTokenLocation(tokenLocalhost);
        configEntity.setTokenExpire(tokenExpire);
        return configEntity;
    }

    @Bean
    public RedisUtil getJedis() {
        return RedisUtil.getRedisUtil(redisHost, redisPort, redisPassword);
    }
}
