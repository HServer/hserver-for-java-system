package com.hserver.system.config;

import com.hserver.system.service.TokenService;
import redis.clients.jedis.Jedis;
import top.hserver.core.interfaces.PermissionAdapter;
import top.hserver.core.ioc.annotation.*;
import top.hserver.core.server.context.Webkit;
import top.hserver.core.server.util.JsonResult;

import java.util.Map;

@Bean
public class PermissionConfig implements PermissionAdapter {
    @Autowired
    private TokenService tokenService;

    @Override
    public void requiresPermissions(RequiresPermissions requiresPermissions, Webkit webkit) throws Exception {
        String token1 = webkit.httpRequest.getHeader("x-access-token");
        String token2 = webkit.httpRequest.getHeader("X-Access-Token");
        if (token2 == null && token1 == null) {
            webkit.httpResponse.sendJson(JsonResult.error("x-access-token：token不存在"));
        } else {
            if (token1 != null) {
                token2 = token1;
            }
            if (!tokenService.requiresPermissions(token2, webkit)) {
                webkit.httpResponse.sendJson(JsonResult.error("token验证无效！"));
            }
        }
    }

    @Override
    public void requiresRoles(RequiresRoles requiresRoles, Webkit webkit)  throws Exception{
        //这里你可以实现一套自己的角色检查算法逻辑，判断，
        //其他逻辑同上
        System.out.println(requiresRoles.value()[0]);
    }

    @Override
    public void sign(Sign sign, Webkit webkit)  throws Exception{
        //这里你可以实现一套自己的接口签名算法检查算法逻辑，判断，
        //其他逻辑同上
        Map requestParams = webkit.httpRequest.getRequestParams();
        String sign1 = webkit.httpRequest.getHeader("sign");
        System.out.println(sign.value());
    }
}
