package com.hserver.system.config;

import top.hserver.core.interfaces.GlobalException;
import top.hserver.core.ioc.annotation.Bean;
import top.hserver.core.server.context.Webkit;
import top.hserver.core.server.util.JsonResult;

/**
 * @author hxm
 */
@Bean
public class WebException implements GlobalException {

    @Override
    public void handler(Throwable throwable, int i, String s, Webkit webkit) {
        throwable.printStackTrace();
        JsonResult result = JsonResult.error("发生了一个错误");
        result.put("data", s);
        webkit.httpResponse.sendJson(result);
    }
}