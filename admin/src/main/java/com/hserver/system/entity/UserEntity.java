package com.hserver.system.entity;


import org.beetl.sql.core.annotatoin.Table;

import java.util.Date;

@Table(name = "sys_user")
public class UserEntity extends BaseEntity {

  private Integer id;

  private String username;
  private String realname;
  private String password;
  private Date birthday;
  private Integer sex;
  private String email;
  private String phone;
  private Integer status;
  private String delFlag;

  private String selectedroles;


  @Override
  public Integer getId() {
    return id;
  }

  @Override
  public void setId(Integer id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getRealname() {
    return realname;
  }

  public void setRealname(String realname) {
    this.realname = realname;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public Date getBirthday() {
    return birthday;
  }

  public void setBirthday(Date birthday) {
    this.birthday = birthday;
  }

  public Integer getSex() {
    return sex;
  }

  public void setSex(Integer sex) {
    this.sex = sex;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  public String getDelFlag() {
    return delFlag;
  }

  public void setDelFlag(String delFlag) {
    this.delFlag = delFlag;
  }

  public String getSelectedroles() {
    return selectedroles;
  }

  public void setSelectedroles(String selectedroles) {
    this.selectedroles = selectedroles;
  }
}
