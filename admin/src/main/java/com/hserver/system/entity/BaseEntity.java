package com.hserver.system.entity;


import java.util.Date;

public class BaseEntity extends ViewModel {

  private Integer id;
  private String createBy;
  private String updateBy;
  private Date createTime;
  private Date upadateTime;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getCreateBy() {
    return createBy;
  }

  public void setCreateBy(String createBy) {
    this.createBy = createBy;
  }

  public String getUpdateBy() {
    return updateBy;
  }

  public void setUpdateBy(String updateBy) {
    this.updateBy = updateBy;
  }

  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }

  public Date getUpadateTime() {
    return upadateTime;
  }

  public void setUpadateTime(Date upadateTime) {
    this.upadateTime = upadateTime;
  }
}
