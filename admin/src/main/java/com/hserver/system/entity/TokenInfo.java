package com.hserver.system.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * redis 缓存使用的相关数据需要存储起来
 */
public class TokenInfo {
    private UserEntity userEntity;
    private List<String> permissionEntities = new ArrayList<>();

    public TokenInfo() {
    }

    public TokenInfo(UserEntity userEntity, List<PermissionEntity> permissionEntities) {
        this.userEntity = userEntity;
        for (PermissionEntity permissionEntity : permissionEntities) {
            //是按钮权限，同时是uri符合标准，
            if (permissionEntity.getIsRoute() != null && 0 == permissionEntity.getIsRoute() && permissionEntity.getPath() != null) {
                this.permissionEntities.add(permissionEntity.getPath());
            } else {
                List<PermissionEntity> children = permissionEntity.getChildren();
                if (children != null) {
                    for (PermissionEntity child : children) {
                        if (child.getIsRoute() != null && 0 == child.getIsRoute() && child.getPath() != null) {
                            this.permissionEntities.add(child.getPath());
                        }
                    }
                }
            }
        }
    }

    public UserEntity getUserEntity() {
        return userEntity;
    }

    public void setUserEntity(UserEntity userEntity) {
        this.userEntity = userEntity;
    }

    public List<String> getPermissionEntities() {
        return permissionEntities;
    }

    public void setPermissionEntities(List<String> permissionEntities) {
        this.permissionEntities = permissionEntities;
    }
}
