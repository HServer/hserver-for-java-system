package com.hserver.system.entity;

import org.beetl.sql.core.annotatoin.Table;

@Table(name = "sys_token")
public class TokenEntity extends BaseEntity {
  private String token;

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }
}
