package com.hserver.system.entity;

import org.beetl.sql.core.annotatoin.Table;

@Table(name = "sys_role")
public class RoleEntity extends BaseEntity {
  private String roleName;
  private String roleCode;
  private String description;

  public String getRoleName() {
    return roleName;
  }

  public void setRoleName(String roleName) {
    this.roleName = roleName;
  }

  public String getRoleCode() {
    return roleCode;
  }

  public void setRoleCode(String roleCode) {
    this.roleCode = roleCode;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
}
