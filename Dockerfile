FROM registry.cn-shenzhen.aliyuncs.com/hserver-base/java8-maven-node:base
WORKDIR /usr/local/src
RUN rm -frv /usr/local/src/*
COPY . /usr/local/src
RUN mvn clean
#前端进行install
WORKDIR /usr/local/src/web
RUN yarn
WORKDIR /usr/local/src
RUN mvn package -Dmaven.test.skip=true
RUN ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
RUN echo 'Asia/Shanghai' >/etc/timezone
ENTRYPOINT ["java","-jar","/usr/local/src/admin/target/hserver-for-java-system.jar"]