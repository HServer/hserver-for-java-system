import { getAction,deleteAction,putAction,postAction} from '@/api/manage'
//角色管理
const addRole = (params)=>postAction("/role/add",params);
const editRole = (params)=>putAction("/role/edit",params);

const checkRoleCode = (params)=>getAction("/role/checkRoleCode",params);
const queryall = (params)=>getAction("/role/queryall",params);

//用户管理
const addUser = (params)=>postAction("/user/add",params);
const editUser = (params)=>putAction("/user/edit",params);
const queryUserRole = (params)=>getAction("/user/queryUserRole",params);
const getUserList = (params)=>getAction("/user/list",params);

const frozenBatch = (params)=>putAction("/user/frozenBatch",params);
//验证用户账号是否唯一
const checkUsername = (params)=>getAction("/user/checkOnlyUser",params);
//改变密码
const changPassword = (params)=>putAction("/user/changPassword",params);

//权限管理
const addPermission= (params)=>postAction("/permission/add",params);
const editPermission= (params)=>putAction("/permission/edit",params);
const getPermissionList = (params)=>getAction("/permission/list",params);
const synchronize = (params)=>getAction("/permission/synchronize",params);


const queryTreeList = (params)=>getAction("/permission/queryTreeList",params);
const queryTreeListForRole = (params)=>getAction("/role/queryTreeList",params);
const queryListAsync = (params)=>getAction("/permission/queryListAsync",params);
const queryRolePermission = (params)=>getAction("/permission/queryRolePermission",params);
const saveRolePermission = (params)=>postAction("/permission/saveRolePermission",params);

const queryPermissionsByUser = (params)=>getAction("/permission/getUserPermissionByToken",params);
const loadAllRoleIds = (params)=>getAction("/permission/loadAllRoleIds",params);
const getPermissionRuleList = (params)=>getAction("/permission/getPermRuleListByPermId",params);
const queryPermissionRule = (params)=>getAction("/permission/queryPermissionRule",params);

// 部门管理
const queryDepartTreeList = (params)=>getAction("/sysdepart/sysDepart/queryTreeList",params);
const queryIdTree = (params)=>getAction("/sysdepart/sysDepart/queryIdTree",params);
const queryParentName   = (params)=>getAction("/sysdepart/sysDepart/queryParentName",params);
const searchByKeywords   = (params)=>getAction("/sysdepart/sysDepart/searchBy",params);
const deleteByDepartId   = (params)=>deleteAction("/sysdepart/sysDepart/delete",params);
//存放位置
const queryWTreeList = (params)=>getAction("/warehouseinfo/wareHouseInfo/queryTreeList",params);
const deleteByWId   = (params)=>deleteAction("/warehouseinfo/wareHouseInfo/delete",params);
const searchByKeywordsW   = (params)=>getAction("/warehouseinfo/wareHouseInfo/searchBy",params);

//日志管理
//const getLogList = (params)=>getAction("/log/list",params);
const deleteLog = (params)=>deleteAction("/log/delete",params);
const deleteLogList = (params)=>deleteAction("/log/deleteBatch",params);

//数据字典
const addDict = (params)=>postAction("/dict/add",params);
const editDict = (params)=>putAction("/dict/edit",params);
//const getDictList = (params)=>getAction("/dict/list",params);
const treeList = (params)=>getAction("/dict/treeList",params);
// const delDict = (params)=>deleteAction("/dict/delete",params);
//const getDictItemList = (params)=>getAction("/dictItem/list",params);
const addDictItem = (params)=>postAction("/dictItem/add",params);
const editDictItem = (params)=>putAction("/dictItem/edit",params);
//const delDictItem = (params)=>deleteAction("/dictItem/delete",params);
//const delDictItemList = (params)=>deleteAction("/dictItem/deleteBatch",params);

//字典标签专用（通过code获取字典数组）
export const ajaxGetDictItems = (code, params)=>getAction(`/dict/getDictItems/${code}`,params);

//系统通告
const doReleaseData = (params)=>getAction("/annountCement/doReleaseData",params);
const doReovkeData = (params)=>getAction("/annountCement/doReovkeData",params);
//获取系统访问量
const getLoginfo = (params)=>getAction("/loginfo",params);

// 查询用户角色表里的所有信息
const queryUserRoleMap = (params)=>getAction("/user/queryUserRoleMap",params);
// 重复校验
const duplicateCheck = (params)=>getAction("/duplicate/check",params);

export {
  // imgView,
  // doMian,
  addRole,
  editRole,
  checkRoleCode,
  addUser,
  editUser,
  queryUserRole,
  getUserList,
  queryall,
  frozenBatch,
  checkUsername,
  changPassword,
  getPermissionList,
  synchronize,
  addPermission,
  editPermission,
  queryTreeList,
  queryListAsync,
  queryRolePermission,
  saveRolePermission,
  queryPermissionsByUser,
  loadAllRoleIds,
  getPermissionRuleList,
  queryPermissionRule,
  queryDepartTreeList,
  queryWTreeList,
  deleteByWId,
  searchByKeywordsW,
  queryIdTree,
  queryParentName,
  searchByKeywords,
  deleteByDepartId,
  deleteLog,
  deleteLogList,
  addDict,
  editDict,
  treeList,
  addDictItem,
  editDictItem,
  doReleaseData,
  doReovkeData,
  getLoginfo,
  queryUserRoleMap,
  duplicateCheck,
  queryTreeListForRole,
}



