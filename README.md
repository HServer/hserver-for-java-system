# HServer-SyStem
    交流QQ群：1065301527
#### 介绍
     该项目是基于HServer服务器开发的一款快速脚手架工具，项目使用了HServer+Redis+Vue+Antd+MYSQL
     如果你对该项目感兴趣，欢迎点赞，项目实现了基本的用户，角色，权限，菜单等功能，为你快速开发项目
     打下了一个最基本的基础，如果对 项目结构或者HServer不熟悉的话，请前往HServer，查询详情
     本项目将会长期更新迭代，会让它成为最稳定的高性能的框架，本项目采用HServer+redis，
     如果你在编写一些接口相关的程序，那么他将是一个不错选择。
     注意本项目完全脱离javaEE标准，不使用spring等相关的包，具体细节请移步到HServer相关文档
     
注意
===     
     
     注意：主分支采用的是Mybatis，另外一个分支是采用的Beetlsql的，更具你需求使用哦，推荐使用Beetlsql，HServer对它做了优化，
     而且它本身性能也不错哦，更多的文档请参考，Beetlsql的文档进行写代码哈
     
#### 部署
    你讲项目git到本地，修改redis，mysql等相关信息，然后对vue的环境进行安装搭建。
    然后你就可以启动项目了。

#### 打包
    当你环境一些搭建好了，如果是idea，直接选择package即可，完成全自动打包，
    将会把vue项目的静态文件，移动到HServer项目下的static，在进行打包

#### 相关效果图
    
![登录](https://gitee.com/HServer/hserver-for-java-system/raw/master/doc/img/login.jpg)
![基础配置](https://gitee.com/HServer/hserver-for-java-system/raw/master/doc/img/set.png)
![用户管理](https://gitee.com/HServer/hserver-for-java-system/raw/master/doc/img/user.png)
![菜单与权限](https://gitee.com/HServer/hserver-for-java-system/raw/master/doc/img/cai.png)
![角色管理](https://gitee.com/HServer/hserver-for-java-system/raw/master/doc/img/role.png)
            